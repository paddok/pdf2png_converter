
// ConvertTDlg.h : header file
//

#pragma once


// CConvertTDlg dialog
class CConvertTDlg : public CDialogEx
{
// Construction
public:
	CConvertTDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CONVERTT_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CString SelectFile(const char szFilter[]);
	afx_msg void OnBnClickedConvTiff();
	BOOL m_bRadio200;
	BOOL m_bRadio300;
	CButton m_radio200;
	CButton m_radio300;
};
