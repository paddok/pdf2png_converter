
// ConvertTDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ConvertT.h"
#include "ConvertTDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "imageconvertDll.h"
#include "AB_PDFLIB.h"


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CConvertTDlg dialog



CConvertTDlg::CConvertTDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CONVERTT_DIALOG, pParent)
	, m_bRadio200(FALSE)
	, m_bRadio300(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CConvertTDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RADIO_DPI1, m_bRadio200);
	DDX_Radio(pDX, IDC_RADIO_DPI2, m_bRadio300);
	DDX_Control(pDX, IDC_RADIO_DPI1, m_radio200);
	DDX_Control(pDX, IDC_RADIO_DPI2, m_radio300);
}

BEGIN_MESSAGE_MAP(CConvertTDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CConvertTDlg::OnBnClickedOk)
	ON_BN_CLICKED(ID_CONV_TIFF, &CConvertTDlg::OnBnClickedConvTiff)
END_MESSAGE_MAP()


// CConvertTDlg message handlers

BOOL CConvertTDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	m_radio200.SetCheck(1);
	m_radio300.SetCheck(0);
	

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CConvertTDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CConvertTDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CConvertTDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CConvertTDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CString inFile;
	CString outFile;
	
	inFile = SelectFile("PDF File\0*.pdf\0All File(*.*)\0*.*\0");

	long beginClock, endClock;
	long totalClock = 0;


	CString outFilePath;
	char sDrive[_MAX_DRIVE] = "\0";
	char sDir[_MAX_DIR] = "\0";
	char sFname[_MAX_FNAME] = "\0";
	char sExt[_MAX_EXT] = "\0";

	// Currently not using
	// Just follow the internal option
	int nDPI = 200;

	if (m_bRadio300)
		nDPI = 300;
	else
		nDPI = 200;


	_splitpath_s(inFile.GetBuffer(), sDrive, _MAX_DRIVE, sDir, _MAX_DIR, sFname, _MAX_FNAME, sExt, _MAX_EXT);

	outFilePath.Format("%s%s", sDrive, sDir);

	beginClock = clock();

	int ret = AB_ConvertPDF2PNG(inFile.GetBuffer());

	endClock = clock();

	totalClock = endClock - beginClock;

	CString resultString = "";

	resultString.Format("Return value:%d, Total converting time : %ld\n", ret, totalClock);
	
	AfxMessageBox(resultString);

	/* 저장폴더 열어줌 */
	ShellExecute(NULL, _T("open"), outFilePath, NULL, NULL, SW_SHOW);

	//CDialogEx::OnOK();
}

CString CConvertTDlg::SelectFile(const char szFilter[])
{
	OPENFILENAME OFN;
	char lpwstrFile[MAX_PATH] = "";
	CString selectedFile = "";


	memset(&OFN, 0, sizeof(OPENFILENAME));
	OFN.lStructSize = sizeof(OPENFILENAME);
	OFN.hwndOwner = NULL;
	OFN.lpstrFilter = szFilter;
	OFN.lpstrFile = lpwstrFile;
	OFN.nMaxFile = 256;
	OFN.lpstrInitialDir = "c:\\";

	if (GetOpenFileName(&OFN) != 0) {
		//sprintf_s(str, "%s 파일을 선택했습니다.", OFN.lpstrFile);
		//MessageBox(NULL, str, "파일 열기 성공", MB_OK);
		selectedFile = OFN.lpstrFile;
	}

	return selectedFile;
}



void CConvertTDlg::OnBnClickedConvTiff()
{
	// TODO: Add your control notification handler code here
	CString inFile;
	CString outFile;

	inFile = SelectFile("TIFF File\0*.tif\0All File(*.*)\0*.*\0");

	long beginClock, endClock;
	long totalClock = 0;

	CString outFilePath;
	char sDrive[_MAX_DRIVE] = "\0";
	char sDir[_MAX_DIR] = "\0";
	char sFname[_MAX_FNAME] = "\0";
	char sExt[_MAX_EXT] = "\0";


	_splitpath_s(inFile.GetBuffer(), sDrive, _MAX_DRIVE, sDir, _MAX_DIR, sFname, _MAX_FNAME, sExt, _MAX_EXT);

	outFilePath.Format("%s%s",sDrive,sDir);

	beginClock = clock();

	int ret = ImageConvert_TIF2TIF_Split(inFile.GetBuffer(), outFilePath.GetBuffer(), sFname);

	endClock = clock();

	totalClock = endClock - beginClock;

	CString resultString = "";

	if (ret == 0)
	{


		resultString.Format("Success, Total converting time : %ld\n", totalClock);
		AfxMessageBox(resultString);

		/* 저장폴더 열어줌 */
		ShellExecute(NULL, _T("open"), outFilePath, NULL, NULL, SW_SHOW);

	}
	else {

		resultString.Format("Fail Return value:%d\n", ret);
		AfxMessageBox(resultString);
	}



}
