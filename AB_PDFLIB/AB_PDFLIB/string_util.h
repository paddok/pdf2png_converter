
#pragma once

#pragma warning(disable:4996)		// Disable 4996 error  (strcpy)

#include <string>

using namespace std;

static char _x2c(char hex_up, char hex_low);
bool isHangulChar(const char ch);
string SUtil_AnsiToUTF8(const char * ansiString);
char* SUtil_UTF8ToAnsi(const char *utf8String);
string SUtil_ReplaceWord(const char* lpszSrc, const char* oldWord, const char* newWord, bool bReplaceOnce = false);
bool SUtil_ReplaceWordEx(const char* pString, string &retStr, const char* src, const char* dst);
size_t SUtil_FindKeyword(const char* string, const char *key);
void SUtil_StrToLower(char* srcStr);
string SUtil_StrToLowerString(char* srcStr);
string SUtil_URLEncode(const char* srcStr);
string SUtil_URLDecode(const char* srcStr);
string SUtil_encodebase64(unsigned char const* bytes_to_encode, unsigned int in_len);
string SUtil_decocebase64(string const& encoded_string);
string SUtil_stringFormat(const string fmt_str, ...);
