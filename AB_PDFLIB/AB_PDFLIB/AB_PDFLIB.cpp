// AB_PDFLIB.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "string_util.h"

/*=============================================================================
 *	 MUPDF를 활용한 PDF -> PNG 변환 라이브러리                                     
 *  MUPDF에서 일부 필수 함수들만 차용했고 쉽게 사용 가능하도록 Wrapping해서 
 *  Export 함수로 작성했다.
 *  예외처리 및 결과값을 리턴받는 부분은 최대한 간단하게 작성했음
 *=============================================================================*/
// mupdf
enum {
	FZ_STORE_UNLIMITED = 0,
	FZ_STORE_DEFAULT = 256 << 20,
};

#define FZ_VERSION "1.15.0"
#define fz_new_context(alloc, locks, max_store) fz_new_context_imp(alloc, locks, max_store, FZ_VERSION)

typedef struct fz_output_s fz_output;

typedef struct fz_alloc_context_s fz_alloc_context;
typedef struct fz_error_context_s fz_error_context;
typedef struct fz_error_stack_slot_s fz_error_stack_slot;
typedef struct fz_warn_context_s fz_warn_context;
typedef struct fz_font_context_s fz_font_context;
typedef struct fz_colorspace_context_s fz_colorspace_context;
typedef struct fz_cmm_engine_s fz_cmm_engine;
typedef struct fz_cmm_instance_s fz_cmm_instance;
typedef struct fz_aa_context_s fz_aa_context;
typedef struct fz_style_context_s fz_style_context;
typedef struct fz_locks_context_s fz_locks_context;
typedef struct fz_tuning_context_s fz_tuning_context;
typedef struct fz_store_s fz_store;
typedef struct fz_glyph_cache_s fz_glyph_cache;
typedef struct fz_document_handler_context_s fz_document_handler_context;
typedef struct fz_output_context_s fz_output_context;
typedef struct fz_context_s fz_context;

typedef struct pdf_document_s pdf_document;

// Mupdf 내에 선언된 참조 Functions 
extern "C" {
	fz_output *fz_stdout(fz_context *ctx);
	fz_context *fz_new_context_imp(const fz_alloc_context *alloc, const fz_locks_context *locks, size_t max_store, const char *version);
	int pdfpages_main(int argc, char **argv);
	int mudraw_main(int argc, char **argv);
	pdf_document *pdf_open_document(fz_context *ctx, const char *filename);
	int pdf_count_pages(fz_context *ctx, pdf_document *doc);
	void pdf_drop_document(fz_context *ctx, pdf_document *doc);
}


//=============================================================================================================

#include <Windows.h>

string selectFile(const char *initialDir = NULL) {

	OPENFILENAME OFN;
	char lpwstrFile[MAX_PATH] = "";

	memset(&OFN, 0, sizeof(OPENFILENAME));
	OFN.lStructSize = sizeof(OPENFILENAME);
	OFN.hwndOwner = NULL;
	OFN.lpstrFilter = "Every File(*.*)\0*.*\0PDF File\0*.pdf\0";
	OFN.lpstrFile = lpwstrFile;
	OFN.nMaxFile = 256;

	if (initialDir)
		OFN.lpstrInitialDir = initialDir;
	else
		OFN.lpstrInitialDir = "c:\\";

	if (GetOpenFileName(&OFN) != 0) {
		//sprintf_s(str, "%s 파일을 선택했습니다.", OFN.lpstrFile);
		//MessageBox(NULL, str, "파일 열기 성공", MB_OK);
		return OFN.lpstrFile;
	}
	else {
		return "";
	}

}// end SelectFile

bool isPDF( char* filePath ) {

	char drive[_MAX_DRIVE] = "\0";
	char dir[_MAX_DIR] = "\0";
	char fname[_MAX_FNAME] = "\0";
	char ext[_MAX_EXT] = "\0";

	// 입력 파일명을 UTF8로
	string utf8Path;
	string utf8OutPath;
	string tempPath;
	string searchFilter;

	// 파일명 Path분리
	_splitpath_s(filePath, drive, dir, fname, ext);

	char outFilePath[_MAX_PATH] = "\0";

	SUtil_StrToLower(ext);

	if (strcmp(ext, ".pdf") != 0 )
	{
		// PDF포맷이 아님
		return false;
	}

	return true;
}// isPDF


int countConvertedFile(string searchFilter) {

	WIN32_FIND_DATA findData;
	HANDLE hFind = NULL;
	int nCnt = 0;

	if (!searchFilter.data())
		return 0;

	hFind = FindFirstFileA(searchFilter.c_str(), &findData);

	if (hFind != INVALID_HANDLE_VALUE)
	{
		do { nCnt++; } while (FindNextFile(hFind, &findData));
		FindClose(hFind);

		return nCnt;
	}

	return 0;

}// end countConvertedFile

int countPDFPages(char *filePath) {

	int fz_optind = 1;
	int totPage = 0;

	string utf8Path = SUtil_AnsiToUTF8(filePath);

	fz_context *ctx = nullptr;
	pdf_document *doc = NULL;

	ctx = fz_new_context(NULL, NULL, FZ_STORE_UNLIMITED);

	if (!ctx) {
		fprintf(stderr, "cannot initialise context\n");
		return 1;
	}


	if (!filePath) {
		fprintf(stderr, "fail to create memory\n");
		return 2;
	}

	doc = pdf_open_document(ctx, utf8Path.c_str());

	if (doc) {
		totPage = pdf_count_pages(ctx, doc);
		pdf_drop_document(ctx, doc);
	}

	return totPage;

}// end countPDFPages

int convertPDF2PNG(char *filePath) {

	int nTotCnt = 0;
	int nParam = 6;
	char** params;

	char drive[_MAX_DRIVE] = "\0";
	char dir[_MAX_DIR] = "\0";
	char fname[_MAX_FNAME] = "\0";
	char ext[_MAX_EXT] = "\0";

	// 입력 파일명을 UTF8로
	string utf8Path;
	string utf8OutPath;
	string tempPath;

	// 파일명 Path분리
	_splitpath_s(filePath, drive, dir, fname, ext);

	char outFilePath[_MAX_PATH] = "\0";
	char searchFilter[_MAX_PATH] = "\0";

	try {

		params = NULL;

		params = new char*[6];

		if (!params)
			throw 100;

		params[0] = new char[6]; if (!params[0]) throw 0;
		params[1] = new char[4]; if (!params[1]) throw 1;
		params[2] = new char[_MAX_PATH]; if (!params[2]) throw 2;
		params[3] = new char[4]; if (!params[3]) throw 3;
		params[4] = new char[6]; if (!params[4]) throw 4;
		params[5] = new char[_MAX_PATH]; if (!params[5]) throw 5;



	}
	catch (int e) {

		// 메모리 생성이 안되면 에러 리턴 

		switch (e) {
		case 5:
			delete[] params[5];
		case 4:
			delete[] params[4];
		case 3:
			delete[] params[3];
		case 2:
			delete[] params[2];
		case 1:
			delete[] params[1];
		case 0:
			delete[] params[0];
		case 100:
			delete[] params;
			return -1;
		}
	}

	// 출력파일명 생성
	sprintf_s(outFilePath, _MAX_PATH, "%s%s%s_%%03d.png", drive, dir, fname);
	sprintf_s(searchFilter, _MAX_PATH, "%s%s%s*.png", drive, dir, fname);

	// UTF8문자열로 변환
	utf8Path = SUtil_AnsiToUTF8(filePath);
	tempPath = SUtil_stringFormat("%s%s%s_%%03d.png", drive, dir, fname);
	utf8OutPath = SUtil_AnsiToUTF8(tempPath.c_str());

	// 파일 변환을 위한 Parameter생성
	strcpy_s(params[0], 6, "draw");
	strcpy_s(params[1], 4, "-o");
	strcpy_s(params[2], _MAX_PATH, utf8OutPath.c_str());
	strcpy_s(params[3], 4, "-r");
	strcpy_s(params[4], 6, "200");
	strcpy_s(params[5], _MAX_PATH, utf8Path.c_str());

	// PDF생성
	mudraw_main(nParam, params);


	// 실제 생성된 파일 개수 Count
	nTotCnt = countConvertedFile(searchFilter);

	// 메모리 해제
	for (int i = 0; i < nParam; i++) {
		delete[] params[i];
		params[i] = NULL;
	}

	delete[] params;



	return nTotCnt;

}// end convertPDF2PNG



//===============================================================================
// DLL Export Functions
//===============================================================================


// 입력되는 파일명의 PDF의 Page수를 리턴
extern "C" __declspec(dllexport) int AB_CountPDFPages(char *pdfFilePath) {

	int fz_optind = 1;
	int totPage = 0;

	if (!isPDF(pdfFilePath))
		return -100;

	totPage = countPDFPages(pdfFilePath);

	return totPage;

}

// 입력되는 PDF파일에 포함된 페이지를 각각 PNG파일로 출력
// Mupdf에서는 내부적으로 파일명을 UTF8로 처리하므로 한글 파일명 등 UTF8로 변환해줘야 한다.
extern "C" __declspec(dllexport) int AB_ConvertPDF2PNG(char *pdfFilePath) {

	if (!isPDF(pdfFilePath))
		return -100;

	int ret = convertPDF2PNG(pdfFilePath);

	return ret;
}

#ifndef DLL_BUILD
int main()
{
    std::cout << "PDF Convert PNG!\n"; 

	char *filePath = NULL;

	filePath = new char[255];

	if (!filePath)
		return 0;

	string imagePath = selectFile("c:\\");

	strcpy_s(filePath, 255, imagePath.c_str());
	
	int nCnt = AB_CountPDFPages(filePath);
	int nRet = AB_ConvertPDF2PNG(filePath);

	delete[] filePath;

}
#endif
