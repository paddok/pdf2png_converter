//===========================================================================
// File Name		: string_util.cpp
//    
// Company			: ABRAIN
// Author			: Sheewon.Song@abrain.co.kr
// File version		: 1.2
// Last modified	: 2019.06.17
//===========================================================================

#include "string_util.h"
#include <comutil.h>								// For BSTR

static const string base64_chars ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

bool isHangulChar(const char ch)
{
	if (ch & 0x80)
		return true;
	else
		return false;

}// end SUtil_isHangulChar

static char _x2c(char hex_up, char hex_low)
{
	char digit;

	digit = 16 * (hex_up >= 'A'
		? ((hex_up & 0xdf) - 'A') + 10 : (hex_up - '0'));
	digit += (hex_low >= 'A'
		? ((hex_low & 0xdf) - 'A') + 10 : (hex_low - '0'));
	return (digit);
}// end _x2c

string SUtil_AnsiToUTF8(const char * ansiString)
{
	int nLen, utf8Len;
	wchar_t *strUnicode = NULL;
	char* pUTF8String = NULL;

	// Ansi -> Unicode
	nLen = MultiByteToWideChar(CP_ACP, 0, ansiString, lstrlen((LPCSTR)ansiString), NULL, NULL);
	strUnicode = new wchar_t[nLen +1];

	if (strUnicode != NULL) {
		memset(strUnicode, 0, sizeof(wchar_t)*(nLen + 1));
		MultiByteToWideChar(CP_ACP, 0, ansiString, lstrlen((LPCSTR)ansiString), strUnicode, nLen);

		// Unicode -> UTF8
		utf8Len = WideCharToMultiByte(CP_UTF8, 0, strUnicode, -1, pUTF8String, 0, NULL, NULL);
		pUTF8String = new char[utf8Len + 1];
		WideCharToMultiByte(CP_UTF8, 0, strUnicode, -1, pUTF8String, utf8Len, NULL, NULL);

		delete[] strUnicode;
	}

	return pUTF8String;

}// ANSI2UTF8


char* SUtil_UTF8ToAnsi(const char *utf8String)
{

	wchar_t *strUnicode = NULL; 
	char	*strMultibyte = NULL;

	// UTF8 -> Unicode
	int nLen = MultiByteToWideChar(CP_UTF8, 0, utf8String, strlen(utf8String), NULL, NULL);
	strUnicode = new wchar_t[nLen+1];

	if (strUnicode != NULL) {

		memset(strUnicode, 0, sizeof(wchar_t)*(nLen + 1));

		MultiByteToWideChar(CP_UTF8, 0, utf8String, strlen(utf8String), strUnicode, nLen);

		// Unicode -> Multibyte
		nLen = WideCharToMultiByte(CP_ACP, 0, strUnicode, -1, NULL, 0, NULL, NULL);
		strMultibyte = new char[nLen + 1];

		if (strMultibyte != NULL)
		{
			memset(strMultibyte, 0, nLen + 1);
			WideCharToMultiByte(CP_ACP, 0, strUnicode, -1, strMultibyte, nLen, NULL, NULL);
			delete[] strUnicode;

			return strMultibyte;
		}
		
	}

	return NULL;

}// end UTF8ToAnsi

bool SUtil_ReplaceWordEx(const char* pString, string &retStr, const char* src, const char* dst)
{

	char* pTemp = NULL;

	retStr = "";

	if (pString == NULL)
		return NULL;

	size_t len = strlen(pString);

	//pTemp = new char[len + 1];

	//strcpy_s(pTemp, len+1, pString);

	size_t pos = SUtil_FindKeyword(pString, src);
	size_t rearLen = len - pos + 1;

	if (pos >= 0)
	{

		char* pFront = NULL;
		char* pRear = NULL;

		size_t srcLen = strlen(src);
		size_t dstLen = strlen(dst);




		if (pos == 0) {

			if (dstLen > 0)
				retStr = dst;

			retStr += (pString + srcLen);
		}
		else {

			pFront = new char[pos + 1];
			pRear = new char[rearLen];

			memset(pFront, 0, pos + 1);
			memset(pRear, 0, rearLen);

#ifdef _DEBUG
			assert(pFront);
			assert(pRear);
#endif
			if (pFront && pRear)
			{
				strncpy_s(pFront, (pos + 1), pString, pos);
				strcpy_s(pRear, rearLen, pString + pos + srcLen);

				retStr = pFront;

				if (dstLen > 0)
					retStr += dst;

				retStr += pRear;

				return true;	// 정상적으로 Replace했을때만 true리턴
			}

			
		}

		
	}

	return false;

}// end SUtil_ReplaceWordEx

// 해당 문자를 한번만 찾아서 Replace시킴
string SUtil_ReplaceWord(const char* lpszSrc, const char* oldWord, const char* newWord, bool bReplaceOnce) {

	string retStr = "";

	if (!lpszSrc || !oldWord || !newWord)
		return "";

	int len = strlen(lpszSrc);
	int oldLen = strlen(oldWord);

	for (int i = 0; i < len;i++) {


		if (*(lpszSrc + i) == *(oldWord) ) {
			// 첫문자가 일치하면 전체 비교

			bool isMatch = true;
			for (int j = 1; j < oldLen; j++)
			{
				if (*(lpszSrc + i + j) != *(oldWord + j)) {
					isMatch = false;
					break;
				}					
			}// end for

			if (isMatch)
			{
				retStr += newWord;

				if (bReplaceOnce) {
					retStr += (lpszSrc + i + oldLen);	// 한번만 Replace
					break;
				}
				else if ( oldLen > 1){
					i += (oldLen-1);					// 계속 Replace
				}

			}
			else {

				retStr += *(lpszSrc + i);

				if (isHangulChar(*(lpszSrc + i)))
				{
					i++;		// 한글이면 2byte 이므로
					retStr += *(lpszSrc + i);
				}

			}

		}
		else {

			// 일치하지 않으면 그냥 문자열에 넣어줌
			retStr += *(lpszSrc + i);

			if (isHangulChar(*(lpszSrc + i)))
			{
				i++;		// 한글이면 2byte 이므로
				retStr += *(lpszSrc + i);
			}
		}

	}// end for

	return retStr;
}// end SUtil_ReplaceWord

size_t SUtil_FindKeyword(const char* string, const char *key) {

	size_t ret = -1;
	size_t keyLen = 0;

	if (!string || !key)
		return ret;

	size_t len = strlen(string);

	keyLen = strlen(key);

	for (size_t i = 0; i < len; i++)
	{
		if (*(string + i) == *(key))
		{
			int j = 0;

			for (; j < keyLen; j++)
			{
				if (*(string + i + j) != *(key + j)) {
					break;
				}
			}

			if (j == keyLen)
			{
				ret = i;
				break;
			}
		}
	}// end for

	return ret;
}// end SUtil_FindKeyword


void SUtil_StrToLower(char* srcStr)
{
	if (srcStr == NULL)
		return;

	size_t len = strlen(srcStr);
	
	for (size_t i = 0; i < len; i++)
		srcStr[i] = tolower(srcStr[i]);

}// end SUtil_StrToLower

string SUtil_StrToLowerString(char* srcStr)
{
	string retStr = "";

	if (srcStr == NULL)
		return "";

	size_t len = strlen(srcStr);

	for (size_t i = 0; i < len; i++)
		retStr += tolower(srcStr[i]);

	return retStr;
}// end SUtil_StrToLower

string SUtil_URLEncode(const char* srcStr)
{
	char *encstr, buf[3];
	unsigned char c;
	int i, j;

	if (srcStr == NULL) 
		return NULL;

	if ((encstr = (char *)malloc((strlen(srcStr) * 3) + 1)) == NULL)
		return NULL;

	for (i = j = 0; srcStr[i]; i++)
	{
		c = (unsigned char)srcStr[i];
		if ((c >= '0') && (c <= '9')) encstr[j++] = c;
		else if ((c >= 'A') && (c <= 'Z')) encstr[j++] = c;
		else if ((c >= 'a') && (c <= 'z')) encstr[j++] = c;
		else if ((c == '@') || (c == '.') || (c == '/') || (c == '\\')
			|| (c == '-') || (c == '_') || (c == ':'))
			encstr[j++] = c;
		else
		{
			sprintf_s(buf, 3, "%02x", c);
			encstr[j++] = '%';
			encstr[j++] = buf[0];
			encstr[j++] = buf[1];
		}

	}// end for

	encstr[j] = '\0';

	return string(encstr);

}// end SUtil_URLEncode


string SUtil_URLDecode(const char* srcStr)
{

	int i, j;
	char *decstr = NULL;

	if (!srcStr) 
		return NULL;

	if ((decstr = (char *)malloc(strlen(srcStr) + 1)) == NULL)
		return NULL;

	for (i = j = 0; srcStr[j]; i++, j++)
	{
		switch (srcStr[j])
		{
		case '+':
			decstr[i] = ' ';
			break;

		case '%':
			decstr[i] = _x2c(srcStr[j + 1], srcStr[j + 2]);
			j += 2;
			break;

		default:
			decstr[i] = srcStr[j];
			break;
		}
	}
	decstr[i] = '\0';

	return string(decstr);

}// end SUtil_URLDecode




static inline bool is_base64(unsigned char c) {
	return (isalnum(c) || (c == '+') || (c == '/'));
}

string SUtil_encodebase64(unsigned char const* bytes_to_encode, unsigned int in_len) {

	string ret;
	int i = 0;
	int j = 0;
	unsigned char char_array_3[3];
	unsigned char char_array_4[4];

	while (in_len--) {
		char_array_3[i++] = *(bytes_to_encode++);
		if (i == 3) {
			char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
			char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
			char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
			char_array_4[3] = char_array_3[2] & 0x3f;

			for (i = 0; (i < 4); i++)
				ret += base64_chars[char_array_4[i]];
			i = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 3; j++)
			char_array_3[j] = '\0';

		char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
		char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
		char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);

		for (j = 0; (j < i + 1); j++)
			ret += base64_chars[char_array_4[j]];

		while ((i++ < 3))
			ret += '=';

	}

	return ret;

}

string SUtil_decocebase64(string const& encoded_string) {

	int in_len = encoded_string.size();
	int i = 0;
	int j = 0;
	int in_ = 0;
	unsigned char char_array_4[4], char_array_3[3];
	string ret;

	while (in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
		char_array_4[i++] = encoded_string[in_]; in_++;
		if (i == 4) {
			for (i = 0; i < 4; i++)
				char_array_4[i] = base64_chars.find(char_array_4[i]);

			char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (i = 0; (i < 3); i++)
				ret += char_array_3[i];
			i = 0;
		}
	}

	if (i) {
		for (j = 0; j < i; j++)
			char_array_4[j] = base64_chars.find(char_array_4[j]);

		char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
		char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);

		for (j = 0; (j < i - 1); j++) ret += char_array_3[j];
	}

	return ret;

}// end SUtil_decocebase64

string SUtil_stringFormat(const string fmt_str, ...) {
	int final_n, n = ((int)fmt_str.size()) * 2; /* Reserve two times as much as the length of the fmt_str */
	std::unique_ptr<char[]> formatted;
	va_list ap;
	while (1) {
		formatted.reset(new char[n]); /* Wrap the plain char array into the unique_ptr */
		strcpy(&formatted[0], fmt_str.c_str());
		va_start(ap, fmt_str);
		final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
		va_end(ap);
		if (final_n < 0 || final_n >= n)
			n += abs(final_n - n + 1);
		else
			break;
	}
	return string(formatted.get());
}


